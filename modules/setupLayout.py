import json

op('mainApp').par.reinitnet.pulse()

print('Loading layout file...')
layoutFile = me.parent().par.Layoutfile
layoutFilePath = '{}/{}'.format(project.folder, layoutFile.eval())
print(layoutFilePath)
with open(layoutFilePath) as fileContents:
    layoutData = json.loads(fileContents.read())
    op('output').par.value0 = layoutData['width']
    op('output').par.value1 = layoutData['height']
    surfaces = layoutData['surfaces']
    op('surfaces').clear()
    for surface, surfaceData in surfaces.items():
        print(surface)
        print(surfaceData)
        # Create each surface
        canvas = surfaceData['canvas']
        x = surfaceData['x']
        y = surfaceData['y']
        width = surfaceData['w']
        height = surfaceData['h']
        interactive = me.parent(2).fetch('canvasses')[canvas]['interactive']
        type = surfaceData['type']
        rows = surfaceData['rows']
        cols = surfaceData['cols']
        op('surfaces').appendRow([canvas, x, y, width, height, interactive, rows, cols, type])
run("op('interactiveReplicator').par.recreateall.pulse()", delayFrames = 180 )
op('textureReplicator').par.recreateall.pulse()
print('Layout file loaded succesfully :)')