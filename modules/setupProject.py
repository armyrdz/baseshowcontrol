import json

print('Starting setup...')

print('Loading project file...')
projectFile = me.parent().par.Projectfile
projectFilePath = '{}/{}'.format(project.folder, projectFile.eval())
print(projectFilePath)
with open(projectFilePath) as fileContents:
    jsonData = json.loads(fileContents.read())
    for eachKey, eachValue in jsonData.items():
        me.parent().store(eachKey, eachValue)
print('Project file loaded succesfully :)')

print('Loading settings file...')
settingsFile = me.parent().par.Settingsfile
settingsFilePath = '{}/{}'.format(project.folder, settingsFile.eval())
print(settingsFilePath)
with open(settingsFilePath) as fileContents:
    # Get machine name
    jsonData = json.loads(fileContents.read())
    machine = jsonData['machine']
    me.parent().store('machine', machine)
    print(machine)
    role = me.parent().fetch('machines')[machine]['role']
    # Configure com
    comToxFile = me.parent().fetch('nodes')[role]['com_tox']
    op('baseCom').par.externaltox = comToxFile
    op('baseCom').par.reinitnet.pulse()
    # Configure output
    op('containerOutput').par.Layoutfile = "data/layouts/" + machine + "Layout.json"
    outputToxFile = me.parent().fetch('nodes')[role]['output_tox']
    op('containerOutput').par.Apptox = outputToxFile
    # Configure data
    dataToxFile = me.parent().fetch('nodes')[role]['data_tox']
    op('baseData').par.externaltox = dataToxFile
    op('baseData').par.reinitnet.pulse()
print('Settings file loaded succesfully :)')

op.Project.par.reinitextensions.pulse()
op('containerOutput/setupLayout').run(delayFrames = 60)

print('Setup complete!')
print('123')
