Base Show Control
Sistema distribuído de show control base para gestionar la repoducción de audio, video e iluminación

Prerequisitos
Windows 10 Home or Pro
- TouchDesigner099.19930 (https://derivative.ca/download) o versión más reciente
- A commercial TouchDesigner License.

Desarrollo y buenas prácticas

Es necesario revisar las siguientes referencias ya que ayudarán a comprender la distribución u construcción del sistema base así como una idea general de la gestión de versionamiento.

 Modular Architectures
 https://mir.works/blog/2019/8/26/td-summit-modular-architectures


 GitHub & External Toxes
 https://mir.works/blog/2019/8/24/td-github-external-toxes

El proyecto se divide en cuatro módulos principalmente, baseCom, baseTools, baseData y containerOutput.

 baseCom: Alberga los nodos de comunicación de datos de entrada.
 baseTools: Herramientas generales por ejemplo monitoro, control de estado a nodos del sistema, selección de idioma, etc.
 baseData: Ésta sección concentra la información general del sistema, es decir datos que podrían considerarse variables globales del sistema.
 containerOutput: Concentra las aplicaciones que puede tener el sistema, por ejemplo: Interfáz de usuario, linea de tiempo, selectores de escenas, máquina de estados, etc.


Historial de versionamientos
v0.0.3
Logging facilities implemented.
v0.0.2
Simplified UI and introduced spanish localization.
v0.0.1
First release, playback of the complete 6:30 minute piece realized on-premise.


### Running

First, open the mwpdAudio.toe file. To open the main control you need to check the data/settings.json file
```json
{
	"machine" : "main"
}
```
and check that the machine value is set to "main". If so run the mwpd.toe file.

To check one client you must change the data/settings.json to
```json
{
	"machine" : "client01"
}
```
Or to client02. Once you change that file you must open again mwpd.toe (without closing the previous one)

This project is configured to run in the same machine. in order to run the project in different machines you must edit projectTest.json with the correct system info.


Créditos
Otto Sánchez -  Creador del sistema

Licienciamiento
This project is licensed under the Cocolab IC License - see the LICENSE.md file for details. License TBD.


This folder contains the first version of the system that will be used to run content in the Projected Desert zone. Some folders and parts of the patches will change according to the new scopes but the main idea of the whole system will be the same





